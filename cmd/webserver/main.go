package main

import (
	"github.com/cloudflare/cloudflare-go"
	homelab_dns "homelab-dns"
	"log"
	"net"
	"net/http"
	"os"
)

func main() {
	resolver := *homelab_dns.NewResolver(net.LookupIP)

	api, err := cloudflare.NewWithAPIToken(os.Getenv("CF_API_KEY"))
	if err != nil {
		log.Fatal(err)
	}
	zoneID := os.Getenv("CF_ZONE_ID")
	client := *homelab_dns.NewClient(api, zoneID)
	referenceUrl := os.Getenv("REFERENCE_URL")
	app := homelab_dns.NewDefaultApp(resolver, client)
	server := homelab_dns.NewAppServer(app, referenceUrl)

	if err := http.ListenAndServe(":8080", server); err != nil {
		log.Fatalf("could not listen on port 5000 %v", err)
	}
}
